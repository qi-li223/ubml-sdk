/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.spi.event;

import io.iec.edp.caf.commons.event.IEventListener;

/**
 * @author zhaoleitr
 */
public interface MdPkgChangedEventListener extends IEventListener {

    /**
     * 元数据包新增后事件
     *
     * @param args 元数据变更参数
     */
    void fireMdPkgAddedEvent(MdPkgChangedArgs args);

    /**
     * 元数据更新后事件
     *
     * @param args 元数据变更参数
     */
    void fireMdPkgChangedEvent(MdPkgChangedArgs args);
}
