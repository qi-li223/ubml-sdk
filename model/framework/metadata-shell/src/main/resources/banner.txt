${AnsiColor.BRIGHT_YELLOW} ${AnsiStyle.BOLD}
Welcome to
//    .__   ________ .___ ____  ___ ______
//    |__| /  _____/ |   |\   \/  / \ \ \ \
//    |  |/   \  ___ |   | \     /   \ \ \ \
//    |  |\    \_\  \|   | /     \    ) ) ) )
//    |__| \______  /|___|/___/\  \  / / / /
//  ==============\/============\_/ /_/_/_/

iGIX Development Kit (${application.version})