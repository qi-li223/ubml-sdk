/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.api.service;

/**
 * Classname MavenUtilsService Description Maven操作工具类 Date 2020/1/9 14:55
 *
 * @author zhongchq
 * @version 1.0
 */
public interface MavenUtilsService {

    /**
     * 获取本地仓库地址
     *
     * @return 本地仓库地址
     */
    String getLocalRepository();

    /**
     * 执行maven命令
     *
     * @param mePath java代码路径
     * @param comd   命令
     * @return 命令是否执行成功
     */
    boolean exeMavenCommand(String mePath, String comd);

}
