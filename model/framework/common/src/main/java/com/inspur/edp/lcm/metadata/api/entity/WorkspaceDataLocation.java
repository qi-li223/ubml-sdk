/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.api.entity;

/**
 * @Classname WorkspaceDataLocation
 * @Description TODO
 * @Date 2019/7/20 15:08
 * @Created by zhongchq
 * @Version 1.0
 */

public class WorkspaceDataLocation {
    public String workspaceDataLocation;

    public String getWorkspaceDataLocation() {
        return workspaceDataLocation;
    }

    public void setWorkspaceDataLocation(String workspaceDataLocation) {
        this.workspaceDataLocation = workspaceDataLocation;
    }

    @Override
    public String toString() {
        return "WorkspaceDataLocation{" +
            "workspaceDataLocation='" + workspaceDataLocation + '\'' +
            '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkspaceDataLocation)) {
            return false;
        }
        WorkspaceDataLocation item = (WorkspaceDataLocation) obj;
        return workspaceDataLocation == null ? item.getWorkspaceDataLocation() == null : workspaceDataLocation.equals(item.getWorkspaceDataLocation());
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (workspaceDataLocation == null ? 0 : workspaceDataLocation.hashCode());
        return result;
    }
}
