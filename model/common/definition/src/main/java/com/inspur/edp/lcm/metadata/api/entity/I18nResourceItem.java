/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.api.entity;

public class I18nResourceItem {

    /**
     * 唯一识别标识
     */
    private String key;
    /**
     * 对应值
     */
    private String value;
    /**
     * 注释
     */
    private String comment;

    public I18nResourceItem() {

    }

    public I18nResourceItem(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof I18nResourceItem)) {
            return false;
        }
        I18nResourceItem item = (I18nResourceItem) obj;
        return key == null ? item.getKey() == null : key.equals(item.getKey());
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (key == null ? 0 : key.hashCode());
        return result;
    }
}
